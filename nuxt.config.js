import { defineNuxtConfig } from "nuxt";
import { resolve, dirname } from "node:path";
import { fileURLToPath } from "url";
import VueI18nPlugin from "@intlify/unplugin-vue-i18n/vite";
import svgLoader from 'vite-svg-loader'
import viteCompression from 'vite-plugin-compression';

export default defineNuxtConfig({
  app: {
    head: {
      title: "HahuJobs",
      htmlAttrs: {
        lang: 'en'
      },
      meta: [
        { "data-n-head": "ssr", charset: "utf-8" },
        { "x-robots-tag": "all" },
        {
          "data-n-head": "ssr",
          name: "viewport",
          content:
            "width=device-width, initial-scale=1",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "description",
          name: "description",
          content:
            "Hey there, ሰላም ነው? Welcome to HaHuJobs the larges data driven job matching and labor market information platform in Ethiopia. With various service deployments to address the Ethiopian labor market needs; we stand at the for front of the local digital job matching industry.",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "og:url",
          property: "og:url",
          content: "https://Hahu-nuxt-3-dep.netlify.app/",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "og:type",
          property: "og:type",
          content: "website",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "og:title",
          property: "og:title",
          content: "HaHuJobs",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "og:description",
          property: "og:description",
          content:
            "Hey there, ሰላም ነው? Welcome to HaHuJobs the larges data driven job matching and labor market information platform in Ethiopia. With various service deployments to address the Ethiopian labor market needs; we stand at the for front of the local digital job matching industry.",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "og:image",
          property: "og:image",
          content:
            "https://res.cloudinary.com/dyut9eifz/image/upload/v1657870128/Hahu/Hahu_MetaCard_e38h2i.png",
        },

        // twitter
        {
          "data-n-head": "ssr",
          "data-hid": "twitter:card",
          property: "twitter:card",
          content: "summary_large_image",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "twitter:domain",
          property: "twitter:domain",
          content: "Hahu-nuxt-3-dep.netlify.app",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "twitter:url",
          property: "twitter:url",
          content: "https://Hahu-nuxt-3-dep.netlify.app/",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "twitter:title",
          property: "twitter:title",
          content: "HaHuJobs",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "twitter:description",
          property: "twitter:description",
          content:
            "Hey there, ሰላም ነው? Welcome to HaHuJobs the larges data driven job matching and labor market information platform in Ethiopia. With various service deployments to address the Ethiopian labor market needs; we stand at the for front of the local digital job matching industry.",
        },
        { "data-hid": "twitter:image", content: "/images/logo_150.png" },
        {
          "data-hid": "twitter:image",
          "data-n-head": "ssr",
          property: "twitter:image",
          content:
            "https://res.cloudinary.com/dyut9eifz/image/upload/v1657870128/Hahu/Hahu_MetaCard_e38h2i.png",
        },
      ],
      link: [
        {
          "data-n-head": "ssr",
          rel: "icon",
          type: "image/x-icon",
          href: "/images/favicon.ico",
        },
        {
          "data-n-head": "ssr",
          rel: "icon",
          type: "image/png",
          sizes: "16x16",
          href: "/images/favicon.ico",
        },
        {
          "data-n-head": "ssr",
          rel: "icon",
          type: "image/png",
          sizes: "32x32",
          href: "/images/favicon.ico",
        },
        {
          "data-n-head": "ssr",
          rel: "icon",
          type: "image/png",
          sizes: "96x96",
          href: "/images/favicon.ico",
        },
        // { rel: "preconnect", href: "https://fonts.gstatic.com" },
        // {
        //   "data-n-head": "ssr",
        //   rel: "stylesheet",
        //   href: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
        // },
        { href: "https://www.google-analytics.com", rel: "preconnect" },

        {
          href: "https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;600;700;800;900&display=swap",
          rel: "stylesheet",
          rel: "preconnect"
        },
        // { rel: "preconnect", href: "https://fonts.googleapis.com" },
        // { href: "https://fonts.googleapis.com/css2?family=Poppins:wght@500;700&display=swap", rel: "stylesheet" }
      ],
      script: [
        {
          src: "https://www.google.com/recaptcha/enterprise.js?render=6LfQM8QhAAAAAFAliVvqFPO4ocdtecr9Jv4nXeV_",
        },
      ],
    },
  },
  runtimeConfig: {
    secret_key: process.env.SECRET_KEY,
    public: {
      recaptcha_id: process.env.RECAPTCHA_ID,
      analytics_secret_key: process.env.MEASUREMENT_ID,
      formspree_id: process.env.FORMSPREE_ID,
      new_formspree_id: process.env.NEW_FORMSPREE_ID
    },
  },
  image: {
    cloudinary: {
      baseURL: "https://res.cloudinary.com/hahu-io/image/upload/v1660721932/hahuIO",
    },
  },
  modules: ["@nuxtjs/color-mode", "@vueuse/nuxt", "@nuxt/image-edge"],
  css: ["@/assets/css/main.css"],
  build: {
    postcss: {
      postcssOptions: require("./postcss.config.js"),
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    },
    transpile: ["@headlessui/vue", "@intlify/unplugin-vue-i18n", "@heroicons/vue"],
  },
  nitro: {
    compressPublicAssets: true,
    prerender: {
      crawlLinks: true
    }
  },
  vite: {
    plugins: [
      viteCompression({ algorithm: 'brotliCompress' }),
      svgLoader(),
      VueI18nPlugin({
        include: [
          resolve(dirname(fileURLToPath(import.meta.url)), "./locales/*.json"),
        ],
      }),
    ],
  },
  colorMode: {
    classPrefix: "",
    classSuffix: "",
  },
});
