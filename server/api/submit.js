export default defineEventHandler(async (event) => {
    const body = await useBody(event);
    const config = useRuntimeConfig();
    const formspree_id = config.public.formspree_id;
    const submitUrl = `https://formspree.io/f/${formspree_id}`;
    let response = null;
    await $fetch(submitUrl, {
        method: "post",
        body: {
            name: body.name,
            phone: body.phone,
            email: body.email,
            subject: body.subject,
            message: body.message,
        }
    })
        .then((data) => {
            console.log("submit response", data);
            response = data;
        })
        .catch(() => {
            console.log("error");
            response = "error";
        });
    return { response };
});
