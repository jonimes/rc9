import VueGtag from 'vue-gtag-next'

export default defineNuxtPlugin((nuxtApp) => {
  const getGDPR = localStorage.getItem('GDPR:accepted');
  const config = useRuntimeConfig()
  nuxtApp.vueApp.use(VueGtag, {
    property: {
      id: config.public.analytics_secret_key
    },
    appName: 'HaHuJobs',
    enabled: getGDPR === 'true',
    pageTrackerScreenviewEnabled: true
  }, nuxtApp.vueApp.router);
});



// import VueGtag from 'vue-gtag'

// export default defineNuxtPlugin((nuxtApp) => {
//     nuxtApp.vueApp.use(VueGtag, {
//         config: {
//             id: 'G-HY9RV07LV5',
//         },
//     }, nuxtApp.$router)
// })